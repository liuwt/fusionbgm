
%% generating
clear;
clc;
load yeast_200.mat; %ppimatrix
N=length(selectgenes);
for i=1:N
    for j=(i+1):N
        ppimatrix(i,j)=0;
    end
end
linksum = length(find(ppimatrix));
[row col s] = find(ppimatrix);
gene_network = [row col];
for t=1:10
    K = 10;
    Indices = crossvalind('Kfold', linksum, K);
    % Prepare the sparse matrix
    % Vectors of row and column indices, for the nonzero elements of the matrix
    sss = (t-1)*K;
    for k=1:10
        train = setdiff((1:linksum),find(Indices==k));
        trainnet = gene_network(train,:);
        linksnum = size(trainnet,1);
        %     test = find(Indices==k);
        %     testnet = gene_network(test,:);
        %     testnum = size(testnet,1);
        row_indice = zeros(linksnum,1);
        col_indice = zeros(linksnum,1);
        % A vector of nonzero values whose indices are specified by the row and
        % column indices
        value = ones(linksnum,1);
        % Row dimension for the resulting matrix
        row_dim = N;
        % Column dimension for the resulting matrix
        col_dim = N;
        % Main Loop
        for i = 1:linksnum
            row_indice(i,1) = trainnet(i,1);
            col_indice(i,1) = trainnet(i,2);
        end
        %Make the sparse matrix
        matrix = sparse(row_indice, col_indice, value, row_dim, col_dim) + sparse(col_indice, row_indice, value, col_dim, row_dim);
        %%
        graph = zeros(N,N);
        for i=1:N
            for j=1:N
                if matrix(i,j)>=1
                    graph(i,j)=1;
                else
                    graph(i,j)=inf;
                end
            end
        end
        
        ppipath = cell(201*100,4);
        j=1;
        for s=1:N
            for d=s:N
                ppipath{j,1}=s;
                ppipath{j,2}=d;
                activeNodes = [];
                for i = 1:N
                    % initialize the farthest node to be itself;
                    farthestPreviousHop(i) = i;     % used to compute the RTS/CTS range;
                    farthestNextHop(i) = i;
                end;
                [path, totalCost, farthestPreviousHop, farthestNextHop] = dijkstra(N, graph, s, d, farthestPreviousHop, farthestNextHop);
                ppipath{j,3}= path;
                ppipath{j,4}= totalCost;
                j=j+1;
            end
        end
        ppipaths{1,sss+k} = ppipath;
        save ppipath_yeast200_10fold.mat ppipaths
    end
end


%% testing

clear;
clc;
load yeast_200.mat;
load ppipaths_yeast200_10fold.mat;

goldmatrix = ppimatrix;
% goldmatrix = ppimatrix + yeastnet + yeastnet';
N=length(selectgenes);
% load ppipath_yeast_10all.mat; %ppipaths
[nbData, nbVar] = size(Data);
correlation1 = zeros(nbData,nbData);
for i=1:nbData
    for j = 1:nbData
        a=corr(Data(i,:)',Data(j,:)');
        if isnan(a)==0
            correlation1(i,j) = a;
        end
    end
end
eval = zeros(10,8);
for k=1:100
    goldmatrix = ppimatrix;
% goldmatrix = ppimatrix + yeastnet + yeastnet';
    graph=zeros(N,N);
    ppipath=ppipaths{1,k};
    for j=1:size(ppipath,1)
        graph(ppipath{j,1},ppipath{j,2})=ppipath{j,4};
        graph(ppipath{j,2},ppipath{j,1})=ppipath{j,4};
    end
    ppnet = graph;    
    ppnet(ppnet==2)=0.5;
    ppnet(ppnet==3)=0.25;
    ppnet(ppnet==4)=0.125;
    ppnet(ppnet>4)=0;
    basetp = length(find(ppnet==1))
    c = 0.85;
    r = 3;
    corrrw = ppnet + abs(correlation1);
    [gammaA t]= mcl(corrrw, c, r);
    Cscore =0.5*corrrw+0.5*corrrw*gammaA;
%     Cscore(ppnet==1)=0;
%     goldmatrix(ppnet==1)=0;
%     pred = zeros(98,6);
%     for t=1:98
%         threshold1=0.01*(t+2-1);
%         graph=zeros(N,N);
%         graph(Cscore>threshold1) = Cscore(Cscore>threshold1);
%         [tp, fp, fn, tn, prec, recall, spec, pval, f1] = matrixCompare(graph, goldmatrix);
%         pred(t,:)=[threshold1 prec recall spec pval f1];
%     end
%     [maxf1 id]=max(pred(:,6));    
%     threshold1=0.01*(id+2-1)
%     threshold1=0.85;
%  threshold1 = 0.90;
%  threshold1 = 0.8;
 threshold1 = 0.5;
    graph=zeros(N,N);
    graph(Cscore>threshold1) = Cscore(Cscore>threshold1);
        graph(ppnet==1)=0;
    goldmatrix(ppnet==1)=0;
    [tp, fp, fn, tn, prec, recall, spec, pval, f1] = matrixCompare(graph, goldmatrix);    
%     tp = tp - basetp;
%     prec = tp/(tp + fp)*100;
%     recall = tp/(tp + fn)*100;    
%     f1 = 2*prec*recall/(prec+recall);
    accuracy = (tp+tn)/(tp+fp+fn+tn)*100;
    eval(k,:)=[tp fp fn tn prec recall f1 accuracy];
end
avgeval = sum(eval,1)/100;
eval(:,1:4)
eval(:,5:8)
avgeval(:,1:4)
avgeval(:,5:8)
% save ppipath_yeast200_10alleval.mat eval avgeval
% save ppipath_yeast_10alleval.mat eval avgeval
