
function [gammaA t]=mcl(ppnet, c, r)

%% intial random walk markov matrix
ppscore = zeros(size(ppnet,1),size(ppnet,2));
for i=1:size(ppnet,1)
    for j = 1:size(ppnet,2)
        if sum(ppnet(i,:))~=0
        ppscore(i,j) = c*ppnet(i,j)/sum(ppnet(i,:))+(1-c)/sum(ppnet(i,:));
        end
    end
end
norm(ppscore,2);
gammaA = ppscore;
gammaA_old =mk_stochastic(gammaA);
for t=1:50
%% expansion
gammaA = ppscore*ppscore;
%% inflation
gammaA = gammaA.^r;
for i=1:size(ppnet,1)
    if sum(gammaA(i,:))~=0
        gammaA(i,:) = gammaA(i,:)./sum(gammaA(i,:));
    end
end
gammaA =mk_stochastic(gammaA);
if norm(gammaA-gammaA_old,2)<10e-10
    break;
end
gammaA_old = gammaA;
end
return