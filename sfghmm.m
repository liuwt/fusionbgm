function [bestcr, bestPix, bestP, bestnbStates, bestBIC, gammaA, Cscore, RCscore, Datas, threshold1, fan_in] = sfghmm(Data, clusters, mixtures, ppnet, c, r, goldmatrix, clue)
% % ghmm model clustering with PPI constraints
%% softclustering
% clusters = 5;
% mixtures = 4;
% c = 0.85;
% r = 3;
[nbData, nbVar] = size(Data);
[Data_id, Centers] = k_means(Data, clusters);
for i=1:nbData
    for j=1:nbVar
        if Data(i,j)==0
            Data(i,j)=Centers(Data_id(i,1),j);
        end
    end
end
correlation1 = zeros(nbData,nbData);
for i=1:nbData
    for j = 1:nbData
        a=corr(Data(i,:)',Data(j,:)');
        if isnan(a)==0
            correlation1(i,j) = a;
        end
    end
end

for d = 1:6
    ppnet(ppnet==d)=0.5^(d-1);
end
ppnet(ppnet==Inf)=0;
corrrw = ppnet + abs(correlation1);
[gammaA t]= mcl(corrrw, c, r);
Cscore =0.5*corrrw+0.5*corrrw*gammaA;
[mixture_id, Centers] = k_means(Data', mixtures);
for k=1:mixtures
    Datas{1,k}=Data(:,find(mixture_id==k));
end
maxIter = nbData;
nbStates = nbData;
Pix = zeros(nbData, nbStates);
Pix(ppnet>0)=1;
Pix(gammaA>=0.05)=1;
Pix = Pix ./ repmat(sum(Pix,2),[1 nbStates]);
bestP = Pix*Pix';
BIC = [];
crs = [];
bestcr = 0;
bestPix = Pix;
bestnbStates = nbStates;
bestBIC = 0;
for iter = 1:maxIter
    [a, b]=find(Pix>clue);
    nbStates = size(unique(b),1)
    Pix = Pix(:,unique(b));
    Pix(isnan(Pix))=0;
    %Compute cumulated posterior probability
    E = sum(Pix);
    %Update the priors
    Priors = sum(Pix)/nbData;
    Pxi = zeros(nbData, nbStates);
    for i=1:nbStates       
        if Priors(i)>0
            for k=1:mixtures
                DATA = Datas{1,k};
                [nbData, nbVar] = size(DATA);
                Mu = zeros(nbVar, nbStates);
                Sigma = zeros(nbVar, nbVar, nbStates);
                %Update the centers
                Mu(:,i) = DATA'*Pix(:,i)/ E(i);
                %Update the covariance matrices
                Data_tmp1 = DATA' - repmat(Mu(:,i),1,nbData);
                Sigma(:,:,i) = (repmat(Pix(:,i)',nbVar, 1) .* Data_tmp1*Data_tmp1') / E(i);
                %% Add a tiny variance to avoid numerical instability
                Sigma(:,:,i) = Sigma(:,:,i) + 1E-5.*diag(ones(nbVar,1));
                %Compute the new probability p(x|i)
                Pxi(:,i) =Pxi(:,i)+gaussPDF(DATA', Mu(:,i), Sigma(:,:,i));
            end
            neighbori = setdiff(1:nbData, i);
            Pxi(:,i) = Pxi(:,i)*exp(-sum(Cscore(neighbori,i).*(1-Pix(neighbori,i))));
        end
    end
    Pxi(isnan(Pxi))=0;
    %Compute posterior probability p(i|x)
    Pix_tmp = repmat(Priors,[nbData 1]).*Pxi;
    Pix = Pix_tmp ./ repmat(sum(Pix_tmp,2),[1 nbStates]);
    Pix(isnan(Pix))=0;
    F = Pxi*Priors';
    F(find(F<realmin)) = realmin;
    loglik = sum(log(F));
    BIC =[BIC -2*loglik+nbStates*(nbVar+nbVar*(nbVar-1)/2)*log(nbData)]
    iter=iter+1;
    Pix(Pix<=clue)=0;
    [a, b]=find(Pix>clue);
    nbStates = size(unique(b),1)
    Pix = Pix(:,unique(b));
    P = Pix*Pix';
    [tp, fp, fn, tn, prec, sens, spec, pval, f1] = matrixCompare(P, goldmatrix);   
    p = tp + fp + fn + tn;
    cc = ((tp+fp)*(tp+fn)+(fn+tn)*(fp+tn))/p;
    cr = (tp+tn-cc)/(p-cc);
    crs = [crs cr];
    if cr>bestcr
        bestcr = cr;
        bestPix = Pix;
        bestP = P;
        bestnbStates = nbStates;
        bestBIC = -2*loglik+nbStates*(nbVar+nbVar*(nbVar-1)/2)*log(nbData);
    end
end
[tp, fp, fn, tn, prec, sens, spec, pval, f1] = matrixCompare(bestP, goldmatrix);
RCscore = 0.5*bestP+0.5*Cscore;
pred = zeros(98,6);
for t=1:98
    threshold1=0.01*(t+2-1);
    graph=zeros(nbData,nbData);
    graph(RCscore>threshold1) = RCscore(RCscore>threshold1);
    [tp, fp, fn, tn, prec, recall, spec, pval, f1] = matrixCompare(graph, goldmatrix);
    pred(t,:)=[threshold1 prec recall spec pval f1];
end
[maxf1 id]=max(pred(:,6));
threshold1=0.01*(id+2-1);
graph=zeros(nbData,nbData);
graph(RCscore>threshold1) = RCscore(RCscore>threshold1);
[tp, fp, fn, tn, prec, recall, spec, pval, f1] = matrixCompare(graph, goldmatrix);
p = tp + fp + fn + tn;
cc = ((tp+fp)*(tp+fn)+(fn+tn)*(fp+tn))/p;
cr = (tp+tn-cc)/(p-cc);
for child_node = 1:nbData
    fan_in(child_node)=max(size(find(RCscore(:,child_node)>threshold1),1),size(find(bestP(:,child_node)>0.1),1));
end
