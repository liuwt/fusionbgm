##created by Wenting Liu, wting.liu.whu@gmail.com

MATLAB implementation of fusion methods on gene expression and PPIN data: Gaussian Hidden Markov Model (GHMM); 
and Bayesian Gaussian Mixture (BGM) model with structural constraints from GHMM. 

The methods are descibed in the paper "Fusing Gene Expression and Transitive Protein-Protein Interaction Data for Gene Regulatory Networks".
https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-019-0695-x

MATLAB functions
GHMM is implemented by the function sfghmm.m
BGM with structural constraints is implemented by the function fusion_bge.m
#10-fold cross-validation on random PPIN 
testppin.m


Extension of methods
In both functions, I note the structural constraints parts, thus it can easily modified as the corresponding model without structural constraints or with other structural constraints, e.g., from other data sources. 
implement algorithms by bgmdifferprior.m

PROBLEMS AND COMMENTS
Please address any problem or comment to wting.liu.whu@gmail.com