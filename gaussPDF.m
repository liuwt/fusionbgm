function prob = gaussPDF(Data, Mu, Sigma)
%
% This function computes the Probability Density Function (PDF) of a
% multivariate Gaussian represented by means and covariance matrix.
%
% Author:	Sylvain Calinon, 2009
%			http://programming-by-demonstration.org
%
% Inputs -----------------------------------------------------------------
%   o Data:  D x N array representing N datapoints of D dimensions.
%   o Mu:    D x K array representing the centers of the K GMM components.
%   o Sigma: D x D x K array representing the covariance matrices of the 
%            K GMM components.
% Outputs ----------------------------------------------------------------
%   o prob:  1 x N array representing the probabilities for the 
%            N datapoints.     

[nbVar,nbData] = size(Data);
if isnan(Mu)==0
Data = Data' - repmat(Mu',nbData,1);
prob = sum((Data*inv(Sigma)).*Data, 2);
prob = exp(-0.5*prob) / sqrt((2*pi)^nbVar * (abs(det(Sigma))+realmin));
else
    prob = zeros(nbData,1);
end

% [nbData, nbVar] = size(Data);
% if isnan(Mu)==0
% Data = Data - repmat(Mu,[nbData,1]);
% prob = sum((Data*inv(Sigma)).*Data, 2);
% prob = exp(-0.5*prob) / sqrt((2*pi)^nbVar * (abs(det(Sigma))+realmin));
% else
%     prob = zeros(nbData,1);
% end

% function [z] = gausspdf(x,mu,sigma);
% 
% % GAUSSPDF  Values of the Gaussian probability density
% %           function
% %
% %    GAUSSPDF(X,MU,SIGMA) returns the likelihood
% %    of the point or set of points X with respect to
% %    a Gaussian process with mean MU and covariance
% %    SIGMA. MU is a 1*D vector (where D is the dimension
% %    of the process), SIGMA is a D*D matrix
% %    and X is a N*D matrix where each row is a
% %    D-dimensional point.
% %
% %    See also MEAN, COV, GLOGLIKE
% 
% [N,D] = size(x);
% if (min(N,D) == 1), x=x(:)'; end;
% 
% invSig = inv(sigma);
% mu = mu(:)';
% 
% x = x-repmat(mu,N,1);
% 
% z = sum( ((x*invSig).*x), 2 );
% 
% z = exp(-0.5*z) / sqrt((2*pi)^D * det(sigma));
% 
