
%% BGM without any constraints
clear;
clc;
load yeast_benchmark.mat;
[nbData, nbVar] = size(Data);
clusters = 5; 
mixtures = 4;
[mixture_id, Centers] = k_means(Data', mixtures);
for k=1:mixtures
    Datas{1,k}=Data(:,find(mixture_id==k));
end
goldmatrix = net_together; 
steps = 10; 
step_iterations = 1000;
dag = zeros(nbData,nbData);
RCscore = ones(nbData,nbData);
for child_node = 1:nbData
    fan_in(child_node)= nbData - 1;
end
tic;    
[Run_fusion] = fusion_bge(Datas,steps,step_iterations, fan_in, RCscore, dag);
fusiontime = toc;
Run = Run_fusion;
DAG = zeros(nbData,nbData);
for i=2:(steps+1)
DAG = DAG + Run.dag{i};
end
DAG = DAG/steps;
save yeast_benchmarkperformonlybgmtime.mat Run DAG fusiontime


%% BGM with constraints from only gmm
clear;
clc;
load yeast_benchmark.mat;
clusters = 5; 
mixtures = 4;
clue = 0.15;
goldmatrix = net_together; 
steps = 10; 
step_iterations = 1000;
[bestcr, bestPix, bestP, bestnbStates, bestBIC, Datas, RCscore, threshold1, fan_in] = sfgmm(Data, clusters, mixtures, goldmatrix, clue);
[Run_fusion, fusiontime] = gmmtobgm(Datas, steps, step_iterations, fan_in, RCscore);
save yeast_benchmarkperformonlygmmtime.mat bestcr bestPix bestP bestnbStates bestBIC Data Datas RCscore threshold1 fan_in Run_fusion fusiontime


%% BGM with constraints from only ppin

% raw PPIN
clear;
clc;
load yeast_benchmark.mat;
clusters = 5; 
mixtures = 4;
ppimatrix(ppimatrix==Inf)=0;
ppimatrix(ppimatrix>0)=1; 
c = 0.85;
r = 3;
goldmatrix = net_together; 
steps = 10; 
step_iterations = 1000;
[Data, Datas, gammaA, Cscore, RCscore, threshold1, fan_in, Run_fusion, Run_imoto, Run_uaiai] = ppintobgm(Data, clusters, mixtures, ppimatrix, ppimatrix, c, r, goldmatrix, steps, step_iterations);
save yeast_benchmarkperformonlyppmatrix.mat Data Datas gammaA Cscore RCscore threshold1 fan_in Run_fusion Run_imoto Run_uaiai

% extend PPIN from subnet information
clear;
clc;
load yeast_benchmark.mat;
clusters = 5; 
mixtures = 4;
ppimatrix(ppimatrix==Inf)=0;
ppimatrix(ppimatrix>0)=1; 
ppnetO(ppnetO==0)=Inf; 
ppnet = ppnetO;
c = 0.85;
r = 3;
goldmatrix = net_together; 
steps = 10; 
step_iterations = 1000;
[Data, Datas, gammaA, Cscore, RCscore, threshold1, fan_in, Run_fusion, Run_imoto, Run_uaiai, fusiontime, imototime, uaiaitime] = ppintobgm(Data, clusters, mixtures, ppimatrix, ppnet, c, r, goldmatrix, steps, step_iterations);
save yeast_benchmarkperformonlyppnetOtime.mat  Data Datas gammaA Cscore RCscore threshold1 fan_in Run_fusion Run_imoto Run_uaiai fusiontime imototime uaiaitime

% extend PPIN from global information
clear;
clc;
load yeast_benchmark.mat;
clusters = 5; 
mixtures = 4;
ppimatrix(ppimatrix==Inf)=0;
ppimatrix(ppimatrix>0)=1; 
ppnetAll(ppnetAll==0)=Inf; 
ppnet = ppnetAll;
c = 0.85;
r = 3;
goldmatrix = net_together; 
steps = 10; 
step_iterations = 1000;
[Data, Datas, gammaA, Cscore, RCscore, threshold1, fan_in, Run_fusion, Run_imoto, Run_uaiai] = ppintobgm(Data, clusters, mixtures, ppimatrix, ppnet, c, r, goldmatrix, steps, step_iterations);
save yeast_benchmarkperformonlyppnetA.mat  Data Datas gammaA Cscore RCscore threshold1 fan_in Run_fusion Run_imoto Run_uaiai

%% BGM with constraints from GHMM with PPIN

% raw PPIN
clear;
clc;
load yeast_benchmark.mat;
clusters = 5; 
mixtures = 4;
ppimatrix(ppimatrix==Inf)=0;
ppimatrix(ppimatrix>0)=1; 
c = 0.85;
r = 3;
clue = 0.15;
goldmatrix = net_together; 
steps = 10; 
step_iterations = 1000;
%% raw ppin :  
ppnet=ppimatrix;
[bestcr, bestPix, bestP, bestnbStates, bestBIC, gammaA, Cscore, RCscore, Datas, threshold1, fan_in] = sfghmm(Data, clusters, mixtures, ppnet, c, r, goldmatrix, clue);
[Run_fusion, Run_imoto, Run_uaiai, fusiontime, imototime, uaiaitime] = ghmmtobgm(Datas, ppimatrix, steps, step_iterations, fan_in, RCscore);
save yeast_benchmarkperformppimatrix.mat  bestcr bestPix bestP bestnbStates bestBIC Data Datas gammaA Cscore RCscore threshold1 fan_in Run_fusion Run_imoto Run_uaiai

% extend PPIN from subnet information
clear;
clc;
load yeast_benchmark.mat;
clusters = 5; 
mixtures = 4;
ppimatrix(ppimatrix==Inf)=0;
ppimatrix(ppimatrix>0)=1; 
ppnetO(ppnetO==0)=Inf; 
ppnet = ppnetO;
c = 0.85;
r = 3;
clue = 0.15;
goldmatrix = net_together; 
steps = 10; 
step_iterations = 1000;
[bestcr, bestPix, bestP, bestnbStates, bestBIC, gammaA, Cscore, RCscore, Datas, threshold1, fan_in] = sfghmm(Data, clusters, mixtures, ppnet, c, r, goldmatrix, clue);
[Run_fusion, Run_imoto, Run_uaiai, fusiontime, imototime, uaiaitime] = ghmmtobgm(Datas, ppimatrix, steps, step_iterations, fan_in, RCscore);
save yeast_benchmarkperformppnetOtime.mat  bestcr bestPix bestP bestnbStates bestBIC Data Datas gammaA Cscore RCscore threshold1 fan_in Run_fusion Run_imoto Run_uaiai fusiontime imototime uaiaitime

% extend PPIN from global information
clear;
clc;
load yeast_benchmark.mat;
clusters = 5; 
mixtures = 4;
ppimatrix(ppimatrix==Inf)=0;
ppimatrix(ppimatrix>0)=1; 
ppnetAll(ppnetAll==0)=Inf; 
ppnet = ppnetAll;
c = 0.85;
r = 3;
clue = 0.15;
goldmatrix = net_together; 
steps = 10; 
step_iterations = 1000;
[bestcr, bestPix, bestP, bestnbStates, bestBIC, gammaA, Cscore, RCscore, Datas, threshold1, fan_in] = sfghmm(Data, clusters, mixtures, ppnet, c, r, goldmatrix, clue);
[Run_fusion, Run_imoto, Run_uaiai, fusiontime, imototime, uaiaitime] = ghmmtobgm(Datas, ppimatrix, steps, step_iterations, fan_in, RCscore);
save yeast_benchmarkperformppnetAll.mat  bestcr bestPix bestP bestnbStates bestBIC Data Datas gammaA Cscore RCscore threshold1 fan_in Run_fusion Run_imoto Run_uaiai
