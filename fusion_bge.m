
function [Run] = fusion_bge(Datas, steps, step_iterations, fan_in, ppscore, DAG)

global T_0;
global my_0;
global v;
global alpha;
global Prior;
Data =[];
for k=1:size(Datas,2)
    Data = [Data Datas{1,k}];
end
[T_0, my_0, v, alpha, Prior] = SET_HYPERPARAMETERS(Data);

n_nodes = size(Data,1);

[log_score] = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas, DAG)

for i=1:steps+1
Run.dag{i}         = 0;
Run.Log_Scores(i)  = 0;
Run.A_EDGE(i)      = 0;
Run.R_EDGE(i)      = 0;
end  
Run.dag{1}         = DAG;
Run.Log_Scores(1)  = log_score;
Run.steps(1)       = 1;

last           = Run.steps(1);          % index of last sampled dags
DAG            = Run.dag{last};         % the current DAG
log_score      = Run.Log_Scores(last);  % the current log score

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output on screen: 
fprintf('\n###########################################################\n')
fprintf('The MCMC-simulation has been started \n')
fprintf('###########################################################\n')
fprintf('Log-likelihood-value of the initial graph: %1.5f \n',log_score)
fprintf('###########################################################\n\n')

%%% Start of the MCMC-simulation
for i = last:(steps+1)
[DAG, log_score,  A_EDGE,  R_EDGE] = MCMC_INNER(step_iterations, DAG, fan_in, log_score,  Datas, ppscore)
Run.dag{i+1}          = DAG;
Run.Log_Scores(i+1)   = log_score;
Run.A_EDGE(i+1)       = Run.A_EDGE(i)       + A_EDGE;
Run.R_EDGE(i+1)       = Run.R_EDGE(i)       + R_EDGE;
Run.steps(1)          = i+1;
end
return;


function [DAG, log_score,  a_EDGE,  r_EDGE] = MCMC_INNER(step_iterations, DAG, fan_in, log_score,  Datas, ppscore)
[n_parents, n_nodes] = size(DAG);
a_EDGE  = 0;
r_EDGE  = 0;
%%%%%% Start the MCMC simulation:
for t=1:step_iterations% PERFORM A STRUCTURE-MCMC MOVE
    nodes_indicis = randperm(n_nodes);
    child_node    = nodes_indicis(1);
    
    old_parents = DAG(:,child_node);
    new_parents = old_parents;
    
    parents_card_old = sum(old_parents);    
    parents_indicis_old = find(old_parents);
    neighbours_old = fan_in(child_node);
    if (parents_card_old<fan_in(child_node))&& fan_in(child_node)~=0        
        [b idx]=sort(ppscore(:,child_node),'descend'); %ppscore(:,child_node)
        indicis = randperm(fan_in(child_node));
        x = idx(indicis(1));
        if x==child_node && fan_in(child_node)>1
            x=idx(indicis(2));
        end
        parent_index = x; % delete or add this parent node        
        new_parents(parent_index,1) = 1 - new_parents(parent_index,1);        
    elseif (parents_card_old>=fan_in(child_node)) && fan_in(child_node)~=0        
        x = randperm(fan_in(child_node));
        x=x(1);
        
        parent_index = parents_indicis_old(x); % delete this parent node
        new_parents(parent_index,1) = 0;%  
        neighbours_old = parents_card_old;
    end
    
    parents_card_new    = sum(new_parents);
    
    if (parents_card_new<fan_in(child_node))
        neighbours_new = fan_in(child_node); % = n_parents-1
    else
        neighbours_new = parents_card_new; % = fan_in
    end
    
    DAG_NEW = DAG;
    DAG_NEW(:,child_node) = new_parents;
    
    local_score_new = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas, DAG_NEW);
    local_score_old = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas, DAG);
    A1 = exp(local_score_new - local_score_old)
    if neighbours_old~=0 && neighbours_new~=0
        A1 = exp(local_score_new - local_score_old + log(neighbours_old) - log(neighbours_new))  
    end    
    
    if isnan(A1)
        A1=1;
    end
    A2=1;
%     if isempty(find(new_parents))==0 && isempty(find(old_parents))==0
%         A2 = exp(-sum(ppscore(find(new_parents==0),child_node))+sum(ppscore(find(old_parents==0),child_node)))
%         %                      Pxi(:,i) = Pxi(:,i)*exp(-sum(ppnet(neighbori,i).*(1-Pix(neighbori,i))));
%         %                 A2 = sum(ppscore(find(new_parents),child_node))/sum(ppscore(find(old_parents),child_node))
%     else
%         A2 = 1;
%     end    
    A = A1*A2   
    
    u = rand(1);    
    if u > min(1,A) % then reject the move:
        % DAG stays the same
        log_score_diff = 0;
        r_EDGE           = r_EDGE + 1;
    else % accept the move:        
        DAG = DAG_NEW;        
        log_score_diff = local_score_new - local_score_old;
        a_EDGE = a_EDGE + 1;
    end    
    log_score = log_score + log_score_diff;    
end
return


function [log_score] = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas, DAG)
% The hyperparameters:
global T_0;
global v;
global alpha;
global my_0;
global Prior;

[n_plus_thrash, n_nodes] = size(DAG);
clear n_plus_thrash;

%Initialisation:
log_score = 0;
% Compute the score for each of the k mixture components:
for k=1:size(Datas, 2)
    DATA = Datas{1,k};
    % DATA = Data;
    log_score_component = 0;
    
    [n_nodes, n_obs_in_component] =  size(DATA);
    
    if(n_obs_in_component==0)
        log_score_component = 0;
    else
        % Compute the score for the current mixture component:
        for node = 1:n_nodes
            % data = DATA{node}{component};
            % Compute T_m for the current combination of node and component:
            S_m = zeros(n_nodes,n_nodes);
            avg = sum(DATA,2)/n_obs_in_component;%mean(Data')'
            for i=1:n_obs_in_component
                S_m = S_m + (DATA(:,i) - avg) * (DATA(:,i) - avg)';
            end
            
            T_m = T_0 + S_m + (v*n_obs_in_component)/(v+n_obs_in_component) * (my_0 - avg) * (my_0 - avg)';
            
            parents_of_node  = find(DAG(:,node));
            parents_and_node = [parents_of_node; node];
            
            [log_score_local_node_nominator]   = Gauss_Score_complete(length(parents_and_node), n_obs_in_component, v, alpha, T_0(parents_and_node,parents_and_node), T_m(parents_and_node,parents_and_node));
            
            [log_score_local_node_denominator] = Gauss_Score_complete(length(parents_of_node),  n_obs_in_component, v, alpha, T_0(parents_of_node,parents_of_node),   T_m(parents_of_node,parents_of_node));
            
            log_score_component = log_score_component + (log_score_local_node_nominator - log_score_local_node_denominator);
        end
    end
    log_score = log_score + log_score_component;
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
function [log_Score_i] = Gauss_Score_complete(n_nodes_in_sub, n_obs, v, alpha, T_0_sub, T_m_sub)

if n_nodes_in_sub==0
    log_Score_i = 0;
else
    if isnan(T_0_sub)==0
        T_0_sub=1;
    end
    if isnan(T_m_sub)==0
        T_m_sub=1;
    end
    sum_1 = (-1)*(n_nodes_in_sub)*(n_obs)/2 * log(2*pi) + (n_nodes_in_sub/2) * log(v/(v+n_obs));
    
    sum_3 = (alpha/2) * log(det(T_0_sub)) + (-1)*(alpha+n_obs)/2 * log(det(T_m_sub));
    
    log_value_a = (alpha         *n_nodes_in_sub/2)*log(2) + (n_nodes_in_sub*(n_nodes_in_sub-1)/4)*log(pi);
    log_value_b = ((alpha+n_obs) *n_nodes_in_sub/2)*log(2) + (n_nodes_in_sub*(n_nodes_in_sub-1)/4)*log(pi);
    
    for i=1:n_nodes_in_sub
        log_value_a = log_value_a + gammaln(( alpha   +1-i)/2);
        log_value_b = log_value_b + gammaln(((alpha+n_obs)+1-i)/2);
    end
    
    log_Score_i = sum_1 + (-1)*(log_value_a - log_value_b) + sum_3;
    
end

return


function [T_0, my_0, v, alpha, Prior] = SET_HYPERPARAMETERS(DATA)

[n_nodes, n_obs] = size(DATA);
% VALUES MAY BE MODIDIED BY A USER: v, alpha, my_0, and B
Prior = zeros(n_nodes,1);
v     = 1;
alpha = n_nodes+2;
my_0  = 0 * ones(n_nodes,1);
v_vec = 1 * ones(n_nodes,1);
B     = zeros(n_nodes,n_nodes);
% COMPUTE PRECISION-MATRIX W
W =  1/v_vec(1);
for i = 1:(n_nodes-1)
    b_vec = B(1:i,(i+1));
    W = [W + (b_vec * b_vec')/v_vec(i+1) , (-1)*b_vec/v_vec(i+1) ; (-1)*b_vec'/v_vec(i+1), 1/v_vec(i+1)];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTE T_0:
T_0   = (v*(alpha-n_nodes-1))/(v+1) * inv(W);
return;