
function [Run] = uaiai_pcomplex_bge(Datas,steps,step_iterations, fan_in, ppscore, DAG)

% global T_0;
% global my_0;
% global v;
% global alpha;
% global Prior;
Data =[];
for k=1:size(Datas,2)
    Data = [Data Datas{1,k}];
end
% [T_0, my_0, v, alpha, Prior] = SET_HYPERPARAMETERS(Data);

n_nodes = size(Data,1);

[log_score] = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas, DAG)

%% intial log_score
A0=zeros(n_nodes, n_nodes);
A0(find(ppscore==1))=1;
A0(find(ppscore==0))=1;
% A0(find(ppscore~=0))=1;
A0(find(ppscore==Inf))=2;
% A=A0;
% pA = log(1);
tau1 = 1;
% DAG = zeros(nbData, nbData);
% % [log_score_prior]=bnrc_prior(DAG, A); 
% log_score = log_score + pA + bnrc_prior(A) + 2*tau1*size(find(DAG==1),1);


for i=1:steps+1
Run.dag{i}         = 0;
Run.Log_Scores(i)  = 0;
Run.A_EDGE(i)      = 0;
Run.R_EDGE(i)      = 0;
end  
Run.dag{1}         = DAG;
% Run.uppp{1}         = A;
Run.Log_Scores(1)  = log_score;
Run.steps(1)       = 1;

last           = Run.steps(1);          % index of last sampled dags
DAG            = Run.dag{last};         % the current DAG
log_score      = Run.Log_Scores(last);  % the current log score

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output on screen: 
fprintf('\n###########################################################\n')
fprintf('The MCMC-simulation has been started \n')
fprintf('###########################################################\n')
fprintf('Log-likelihood-value of the initial graph: %1.5f \n',log_score)
fprintf('###########################################################\n\n')

%%% Start of the MCMC-simulation
for i = last:(steps+1)
[DAG, log_score,  A_EDGE,  R_EDGE] = HC_INNER(step_iterations, DAG, fan_in, log_score,  Datas, ppscore, A0, tau1);
% [A, DAG, log_score] = update_database(A, DAG, log_score);
Run.dag{i+1}          = DAG;
% Run.uppp{i+1}         = A;
Run.Log_Scores(i+1)   = log_score;
Run.A_EDGE(i+1)       = Run.A_EDGE(i)       + A_EDGE;
Run.R_EDGE(i+1)       = Run.R_EDGE(i)       + R_EDGE;
Run.steps(1)          = i+1;
end
return;


function [DAG, log_score,  a_EDGE,  r_EDGE] = HC_INNER(step_iterations, DAG, fan_in, log_score, Datas, ppscore, A0, tau1)


[n_parents, n_nodes] = size(DAG);
a_EDGE  = 0;
r_EDGE  = 0;
% tau1 = 1;
%%%%%% Start the MCMC simulation:
for t=1:step_iterations% PERFORM A STRUCTURE-MCMC MOVE
    nodes_indicis = randperm(n_nodes);
    child_node    = nodes_indicis(1);
    
    old_parents = DAG(:,child_node);
    new_parents = old_parents;
    
    parents_card_old = sum(old_parents);
    parents_indicis_old = find(old_parents);
    neighbours_old = fan_in(child_node);
    if (parents_card_old<fan_in(child_node))&& fan_in(child_node)~=0
        [b idx]=sort(ppscore(:,child_node),'descend'); %ppscore(:,child_node)
        indicis = randperm(fan_in(child_node));
        x = idx(indicis(1));
        if x==child_node && fan_in(child_node)>1
            x=idx(indicis(2));
        end
        parent_index = x; % delete or add this parent node
        new_parents(parent_index,1) = 1 - new_parents(parent_index,1);
    elseif (parents_card_old>=fan_in(child_node)) && fan_in(child_node)~=0
        x = randperm(fan_in(child_node));
        x=x(1);
        
        parent_index = parents_indicis_old(x); % delete this parent node
        new_parents(parent_index,1) = 0;%
        neighbours_old = parents_card_old;
    end
    
    parents_card_new    = sum(new_parents);
    
    if (parents_card_new<fan_in(child_node))
        neighbours_new = fan_in(child_node); % = n_parents-1
    else
        neighbours_new = parents_card_new; % = fan_in
    end
    
    DAG_NEW = DAG;
    DAG_NEW(:,child_node) = new_parents;
    
    local_score_new = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas, DAG_NEW)+ 2*tau1*size(find(DAG_NEW==1),1)%+bnrc_prior(A);
    local_score_old = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas, DAG)+ 2*tau1*size(find(DAG==1),1)%+bnrc_prior(A);
    
    %%hillclibming ppcomplex
    log_score_diff = local_score_new - local_score_old;
    if log_score_diff > 0
         % accept the move:
        DAG = DAG_NEW;
        a_EDGE = a_EDGE + 1;
        log_score = log_score + log_score_diff;
        if new_parents(parent_index,1)==1 && A0(parent_index, child_node)~=0
            %the move is adding a parent_index from p-p lists
            %construct a protein complex observations instead of (parent_index, child_node) by pca
            %             Data =[];
            % for k=1:size(Datas,2)
            %     Data = [Data Datas{1,k}];
            % end
            left = setdiff(1:n_nodes, [parent_index child_node]);
            for k=1:size(Datas,2)
                Data = Datas{1,k};
                nbVar = size(Data,2);
                pcx = zeros(1,nbVar)
                pcomplex=[Data(parent_index,:); Data(child_node,:)];
                S_m = zeros(2,2);
                for i=1:nbVar
                    S_m = S_m + (pcomplex(:,i) - mean(pcomplex')') * (pcomplex(:,i) - mean(pcomplex')')';
                end
                %[V,D] = eigs(A) returns a diagonal matrix D of A's six largest magnitude eigenvalues
                % and a matrix V whose columns are the corresponding eigenvectors.
                [V,D] = eigs(S_m/nbVar);
                for i=1:nbVar
                    pcx(1,i) = V(:,1)'*((pcomplex(:,i) - mean(pcomplex')'))
                end
                
                %check whether pcx works better than only using parent_index or child_node;
                
                Data_new = [Data(left,:); pcx];
                Datas_new{1,k} = Data_new;
            end
            DAG_NEW2 = [DAG(left,left) DAG(left,parent_index)+DAG(left,child_node); DAG(parent_index,left)+DAG(child_node,left) 1];
            local_score_new_new = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas_new, DAG_NEW2)+ 2*tau1*size(find(DAG_NEW2==1),1);
            log_score_diff2 = local_score_new_new - local_score_new;
            %             last = size(Data_new,1);
            %             local_score_new_new = bnrc_data(Datas_new, last, DAG_NEW)+ 2*tau1*size(find(DAG_NEW==1),1);
            %             local_score_parent = bnrc_data(Datas, parent_index, DAG);
            %             log_score_diff = local_score_new_new - local_score_new - local_score_parent;
            if log_score_diff2 > 0
                % accept the complex:
                %                 DAG = DAG_NEW2;
                %                 Datas = Datas_new;
                DAG([parent_index child_node],[parent_index child_node])=ones(2,2);
                a_EDGE = a_EDGE + 1;
                pcomp = [pcomp; [parent_index child_node]];
                log_score = log_score + log_score_diff2;
            end
        end
    else
        r_EDGE           = r_EDGE + 1;
    end
    
%     %%hillclimbing
%     log_score_diff = local_score_new - local_score_old;
%     if log_score_diff < 0
%         % accept the move:
%         DAG = DAG_NEW;
%         a_EDGE = a_EDGE + 1;
%         log_score = log_score + log_score_diff;
%     else
%         r_EDGE           = r_EDGE + 1;
%     end
    
    %%mcmc
    %
    %     A1 = exp(local_score_new - local_score_old)
    %     if neighbours_old~=0 && neighbours_new~=0
    %         A1 = exp(local_score_new - local_score_old + log(neighbours_old) - log(neighbours_new))
    %     end
    %
    %     if isnan(A1)
    %         A1=1;
    %     end
    %     A2=1;
    % %     if isempty(find(new_parents))==0 && isempty(find(old_parents))==0
    % %         A2 = exp(-sum(ppscore(find(new_parents==0),child_node))+sum(ppscore(find(old_parents==0),child_node)))
    % %         %                      Pxi(:,i) = Pxi(:,i)*exp(-sum(ppnet(neighbori,i).*(1-Pix(neighbori,i))));
    % %         %                 A2 = sum(ppscore(find(new_parents),child_node))/sum(ppscore(find(old_parents),child_node))
    % %     else
    % %         A2 = 1;
    % %     end
    %     A = A1*A2
    %
    %     u = rand(1);
    %     if u > min(1,A) % then reject the move:
    %         % DAG stays the same
    %         log_score_diff = 0;
    %         r_EDGE           = r_EDGE + 1;
    %     else % accept the move:
    %         DAG = DAG_NEW;
    %         log_score_diff = local_score_new - local_score_old;
    %         a_EDGE = a_EDGE + 1;
    %     end
    %     log_score = log_score + log_score_diff;
end
return


% 
% function [log_score_component]= bnrc_data(Data, node, DAG)
% % [log_score_component]=bnrc_data(parents, child_node, Data, Pix_best, Mu_best, Sigma_best)
% % The hyperparameters:
% % global T_0;
% % global v;
% % global alpha;
% % global my_0;
% % global Prior;
% [T_0, my_0, v, alpha, Prior] = SET_HYPERPARAMETERS(Data);
% [nbData, nbVar] = size(Data);
% % log_score_component = 0;
% if(nbVar==0)
%     log_score_component = 0;
% else    
%     % Compute the score for the current mixture component:
% %     for node = 1:nbVar
%         % Compute T_m for the current combination of node and
%         % component:
%         S_m = zeros(nbData,nbData);        
%         for i=1:nbVar
%             S_m = S_m + (Data(:,i) - mean(Data')') * (Data(:,i) - mean(Data')')';
%         end        
%         T_m = T_0 + S_m + (v*nbVar)/(v+nbVar) * (my_0 - mean(Data')') * (my_0 - mean(Data')')';
%         
%         parents_of_node  = find(DAG(:,node));
%         nonparents_of_node = find(DAG(:,node)==0);
%         %         parents_and_node = [parents_of_node; n_plus];
%         parents_and_node = [parents_of_node; node];
%         [log_score_local_node_nominator]   = Gauss_Score_complete(length(parents_and_node), nbVar, v, alpha, T_0(parents_and_node,parents_and_node), T_m(parents_and_node,parents_and_node));
%         
%         [log_score_local_node_denominator] = Gauss_Score_complete(length(parents_of_node),  nbVar, v, alpha, T_0(parents_of_node,parents_of_node),   T_m(parents_of_node,parents_of_node));
%         
%         log_score_component = -2*(log_score_local_node_nominator - log_score_local_node_denominator)%-2*sum(log(ppscore(parents_of_node,node)));%+2*sum(log(ppscore(nonparents_of_node,node)));    
% end
% return
% 


% 
% function [DAG, log_score, a_EDGE, r_EDGE, pcmp] = strucpcomplex(step_iterations, DAG, fan_in, log_score, Data, ppscore, parents_candidate, A0, pcmp);
% %[DAG, log_score, a_EDGE, r_EDGE, pcomp] = strucpcomplex(step_iterations, DAG, fan_in, log_score, Data, ppscore, parents_candidate, A0, pcomp);
% % [DAG, log_score, A, a_EDGE, r_EDGE] = struc_hc(step_iterations, DAG, fan_in, log_score, Data, A, A0, Pix_best, Mu_best, Sigma_best); 
% 
% [nbData, nbVar] = size(Data);
% [n_parents, n_nodes] = size(DAG);
% a_EDGE  = 0;
% r_EDGE  = 0;
% tau1 = 1;
% %%%%%% Start the MCMC simulation:
% for t=1:step_iterations    
%     %     if(x<=p_0) % PERFORM A STRUCTURE-MCMC MOVE    
%     %randomly choose a child_node
%     nodes_indicis = randperm(n_nodes);
%     child_node    = nodes_indicis(1);    
%     %record old parents
%     old_parents = DAG(:,child_node);
%     new_parents = old_parents;    
%     parents_card_old = sum(old_parents);    
%     parents_indicis_old = find(old_parents);  
%     %update parents
%     if (parents_card_old<fan_in(1,child_node)); 
%         %randomly choose a parent_index to update
%         %% no parents constraints
% %         neighbours_old = n_parents;        
%         indicis = randperm(n_parents);
%         x = indicis(1); 
%         %% having parents constraints
% %         [parents_candidate]=process_clusters(Priors_best, Pix_best);
% %         neighbours_old = parents_candidate{child_node,1};        
% %         indicis = randperm(size(parents_candidate{child_node,1},2));
% %         indicis = randperm(size(parents_candidate{child_node,1},2));
% %         x = parents_candidate{child_node,1}(indicis(1));
% %         
%         parent_index = x; 
%         % delete or add this parent node parent_index       
%         new_parents(parent_index,1) = 1 - new_parents(parent_index,1);   
%         
%         % accept or reject new_parents
%         % check bnrc(new_parents, child_node) reduce or not from bnrc(old_parents, child_node)
%         
%     else %(parents_card_old>fan_in(1,child_node))
%         % randomly delete a parent node parent_index
%         x = randperm(fan_in(1,child_node));
%         x = x(1);
%         parent_index = parents_indicis_old(x); % delete this parent node        
%         new_parents(parent_index,1) = 0;        
% %         neighbours_old = parents_card_old; % = fan_in
%     end 
%     % accept or reject new_parents
%     % check bnrc(new_parents, child_node) reduce or not from bnrc(old_parents, child_node)
%     
% %     parents_card_new    = sum(new_parents);    
% %     if (parents_card_new<fan_in(1,child_node))
% %         neighbours_new = fan_in(1,child_node); % = n_parents-1
% %     else
% %         neighbours_new = parents_card_new; % = fan_in
% %     end    
%     DAG_NEW = DAG;
%     DAG_NEW(:,child_node) = new_parents;  
%     % check bnrc(new_parents, child_node) reduce or not from bnrc(old_parents, child_node)
% %     local_score_new =bnrc_data(new_parents, child_node, Data, Pix_best, Mu_best, Sigma_best);
% %     local_score_old =bnrc_data(old_parents, child_node, Data, Pix_best, Mu_best, Sigma_best);
%         local_score_new = bnrc_data(Data, child_node, DAG_NEW)+ 2*tau1*size(find(DAG_NEW==1),1);
%         local_score_old = bnrc_data(Data, child_node, DAG)+ 2*tau1*size(find(DAG==1),1);
%     %     local_score_new =greedyhc(Data, DAG_NEW, A0, A0, Pix_best, Mu_best, Sigma_best);
%     %     local_score_old =greedyhc(Data, DAG, A0, A0, Pix_best, Mu_best, Sigma_best);
%     log_score_diff = local_score_new - local_score_old;
%     if log_score_diff < 0
%          % accept the move:
%         DAG = DAG_NEW;
%         a_EDGE = a_EDGE + 1;
%         log_score = log_score + log_score_diff;
%         if new_parents(parent_index,1)==1 && A0(parent_index, child_node)==1
%             %the move is adding a parent_index from p-p lists
%             %construct a protein complex observations instead of (parent_index, child_node) by pca 
%             pcx = zeros(1,nbVar)
%             pcomplex=[Data(parent_index,:); Data(child_node,:)];
%             S_m = zeros(2,2);        
%             for i=1:nbVar
%             S_m = S_m + (pcomplex(:,i) - mean(pcomplex')') * (pcomplex(:,i) - mean(pcomplex')')';
%             end 
%             %[V,D] = eigs(A) returns a diagonal matrix D of A's six largest magnitude eigenvalues
%             % and a matrix V whose columns are the corresponding eigenvectors.
%             [V,D] = eigs(S_m/nbVar);
%             for i=1:nbVar
%             pcx(1,i) = V(:,1)'*((pcomplex(:,i) - mean(pcomplex')'))
%             end
%             
%             %check whether pcx works better than only using parent_index or child_node;            
%             left = setdiff(1:nbData, [parent_index child_node]);
%             Data_new = [Data(left,:); pcx];
%             DAG_NEW = [DAG(left,left) DAG(left,parent_index)+DAG(left,child_node); DAG(parent_index,left)+DAG(child_node,left) 1];
%             last = size(Data_new,1);
%             local_score_new_new = bnrc_data(Data_new, last, DAG_NEW)+ 2*tau1*size(find(DAG_NEW==1),1);            
%             local_score_parent = bnrc_data(Data, parent_index, DAG);            
%             log_score_diff = local_score_new_new - local_score_new - local_score_parent;
%             if log_score_diff < 0
%              % accept the complex:
%                 DAG = DAG_NEW;
%                 Data = Data_new;
%                 [nbData, nbVar] = size(Data);
%                 a_EDGE = a_EDGE + 1;
%                 pcomp = [pcomp; [parent_index child_node]];
%                 log_score = log_score + log_score_diff;
%             end          
%         end
%     else       
%         r_EDGE           = r_EDGE + 1;
%     end
% end
% return 


% 
% function [A, DAG, log_score] = update_database(A, DAG, log_score_old)
% pd1 = 0.9;
% pd0 = 0.5;
% operation = 0;
% priorA = bnrc_prior(A);
% while operation == 1 && DAG~=A % if isempty(candrow)==0
%     % candidate list that not agree with A
%     [candrow candcol] = find(DAG~=A);
%     % [DAG, log_score_old, A_EDGE, R_EDGE] = struc_hc(step_iterations, DAG, fan_in, log_score, Data, ppscore, parents_candidate);
%     % local_score_old = log_score_old + bnrc_prior(A);
%     
%     local_score_new = zeros(size(candrow,1),1);
%     priorA_temp = zeros(size(candrow,1),1);
%     
%     for i=1:size(candrow,1)
%         A_temp = A;
%         A_temp (candrow(i),candcol(i))=DAG(candrow(i),candcol(i));
%         priorA_temp(i) = bnrc_prior(A_temp);
%         pA = log(1);   %transition probability
%         if A(candrow(i),candcol(i))==0 && A_temp(candrow(i),candcol(i))~=0
%             pA = pA + log(pd0);
%         elseif A(candrow(i),candcol(i))~=0 && A_temp(candrow(i),candcol(i))==0
%             pA = pA + log(1-pd1);    %log(1-pd1);
%         end
%         local_score_new(i) = log_score_old + pA + priorA_temp(i);
%     end
%     % [C,I] = min(...) finds the indices of the minimum values of A, and returns them in output vector I
%     [C,I] = min(local_score_new);
%     log_score_diff = local_score_new(I) - local_score_old - priorA;
%     if log_score_diff <0
%         %accept the change
%         A(candrow(I),candcol(I)) = DAG(candrow(I),candcol(I));
%         log_score = local_score_new(I);
%         priorA = priorA_temp(I);
%     else
%         log_score = log_score_old + priorA;
%         DAG = DAG;
%         A = A;
%         operation = 1;
%     end    
% end
% return



% 
% function [log_score]=bnrc_prior(A)
% tau1 = 1;
% tau2 = 25;
% 
% z1 = size(find(A==1),1);
% z0 = size(find(A==0),1);%unknown
% z2 = size(find(A==2),1);%size(find(A>1),1);
% Z = 0;
% for alpha = 0:z1
% %     Z = Z+2^z0*nchoosek(z1, alpha)*exp(-alpha*tau1);
%     for beta = 0:z2
%         Z = Z+2^z0*nchoosek(z1, alpha)*nchoosek(z2, beta)*exp(-alpha*tau1-beta*tau2);
%     end
% end
% log_score = 2*log(Z);
% % % z1 = size(find(A==1),1);
% % % z0 = size(find(A==0),1);%unknown
% % % z2 = size(find(A==Inf),1);%size(find(A>1),1);
% % % Z = 0;
% % % for alpha = 0:z1
% % %     for beta = 0:z2
% % %         Z = Z+2^z0*nchoosek(z1, alpha)*nchoosek(z2, beta)*exp(-alpha*tau1-beta*tau2);
% % %     end
% % % end
% 
% % % function [log_score]=bnrc_prior(G, A)
% % % tau1 = 0.25;
% % % tau2 = 0.5;
% % edgescore = tau1*size(find(G==1),1);
% % % pd1 = 0.9;
% % % pd0 = 0.5;
% % % pA = log(1);
% % % edgescore = 0;
% % % % j = child_node;
% % % for i=1:nbData
% % %     for j=1:nbData
% % %         if  A0(i,j)==0 && A(i,j)==0
% % %             pA = pA + log(1-pd0);
% % %         elseif A0(i,j)==0 && A(i,j)~=0
% % %             pA = pA + log(pd0);
% % %         elseif A0(i,j)~=0 && A(i,j)~=0
% % %             pA = pA + log(pd1);
% % %         elseif A0(i,j)~=0 && A(i,j)==0
% % %             pA = pA + log(1-pd1);
% % %         end
% % %         if G(i,j)==1 && A(i,j)==0
% % %             edgescore = edgescore+ pd0;
% % %         elseif G(i,j)==1 && A(i,j)==1
% % %             edgescore = edgescore+ pd1;
% % %         end            
% % %     end
% % % end
% % % log_score = 2*log(Z)+2*edgescore+pA;
% % log_score = 2*log(Z)+2*edgescore;
% return




function [log_score] = COMPUTE_LIKELIHOOD_OF_DAG_k_ONLY(Datas, DAG)

Data =[];
for k=1:size(Datas,2)
    Data = [Data Datas{1,k}];
end
% [T_0, my_0, v, alpha, Prior] = SET_HYPERPARAMETERS(Data);

[n_nodes, n_obs] = size(Data);
% VALUES MAY BE MODIDIED BY A USER: v, alpha, my_0, and B
Prior = zeros(n_nodes,1);
v     = 1;
alpha = n_nodes+2;
my_0  = 0 * ones(n_nodes,1);
v_vec = 1 * ones(n_nodes,1);
B     = zeros(n_nodes,n_nodes);
% COMPUTE PRECISION-MATRIX W
W =  1/v_vec(1);
for i = 1:(n_nodes-1)
    b_vec = B(1:i,(i+1));
    W = [W + (b_vec * b_vec')/v_vec(i+1) , (-1)*b_vec/v_vec(i+1) ; (-1)*b_vec'/v_vec(i+1), 1/v_vec(i+1)];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTE T_0:
T_0   = (v*(alpha-n_nodes-1))/(v+1) * inv(W);



% % The hyperparameters:
% global T_0;
% global v;
% global alpha;
% global my_0;
% global Prior;

[n_plus_thrash, n_nodes] = size(DAG);
clear n_plus_thrash;

%Initialisation:
log_score = 0;
% Compute the score for each of the k mixture components:
for k=1:size(Datas, 2)
    DATA = Datas{1,k};
    % DATA = Data;
    log_score_component = 0;
    
    [n_nodes, n_obs_in_component] =  size(DATA);
    
    if(n_obs_in_component==0)
        log_score_component = 0;
    else
        % Compute the score for the current mixture component:
        for node = 1:n_nodes
            % data = DATA{node}{component};
            % Compute T_m for the current combination of node and component:
            S_m = zeros(n_nodes,n_nodes);
            avg = sum(DATA,2)/n_obs_in_component;%mean(Data')'
            for i=1:n_obs_in_component
                S_m = S_m + (DATA(:,i) - avg) * (DATA(:,i) - avg)';
            end
            
            T_m = T_0 + S_m + (v*n_obs_in_component)/(v+n_obs_in_component) * (my_0 - avg) * (my_0 - avg)';
            
            parents_of_node  = find(DAG(:,node));
            parents_and_node = [parents_of_node; node];
            
            [log_score_local_node_nominator]   = Gauss_Score_complete(length(parents_and_node), n_obs_in_component, v, alpha, T_0(parents_and_node,parents_and_node), T_m(parents_and_node,parents_and_node));
            
            [log_score_local_node_denominator] = Gauss_Score_complete(length(parents_of_node),  n_obs_in_component, v, alpha, T_0(parents_of_node,parents_of_node),   T_m(parents_of_node,parents_of_node));
            
            log_score_component = log_score_component + (log_score_local_node_nominator - log_score_local_node_denominator);
        end
    end
    log_score = log_score + log_score_component;
end

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
function [log_Score_i] = Gauss_Score_complete(n_nodes_in_sub, n_obs, v, alpha, T_0_sub, T_m_sub)

if n_nodes_in_sub==0
    log_Score_i = 0;
else
    if isnan(T_0_sub)==0
        T_0_sub=1;
    end
    if isnan(T_m_sub)==0
        T_m_sub=1;
    end
    sum_1 = (-1)*(n_nodes_in_sub)*(n_obs)/2 * log(2*pi) + (n_nodes_in_sub/2) * log(v/(v+n_obs));
    
    sum_3 = (alpha/2) * log(det(T_0_sub)) + (-1)*(alpha+n_obs)/2 * log(det(T_m_sub));
    
    log_value_a = (alpha         *n_nodes_in_sub/2)*log(2) + (n_nodes_in_sub*(n_nodes_in_sub-1)/4)*log(pi);
    log_value_b = ((alpha+n_obs) *n_nodes_in_sub/2)*log(2) + (n_nodes_in_sub*(n_nodes_in_sub-1)/4)*log(pi);
    
    for i=1:n_nodes_in_sub
        log_value_a = log_value_a + gammaln(( alpha   +1-i)/2);
        log_value_b = log_value_b + gammaln(((alpha+n_obs)+1-i)/2);
    end
    
    log_Score_i = sum_1 + (-1)*(log_value_a - log_value_b) + sum_3;
    
end

return


function [T_0, my_0, v, alpha, Prior] = SET_HYPERPARAMETERS(DATA)

[n_nodes, n_obs] = size(DATA);
% VALUES MAY BE MODIDIED BY A USER: v, alpha, my_0, and B
Prior = zeros(n_nodes,1);
v     = 1;
alpha = n_nodes+2;
my_0  = 0 * ones(n_nodes,1);
v_vec = 1 * ones(n_nodes,1);
B     = zeros(n_nodes,n_nodes);
% COMPUTE PRECISION-MATRIX W
W =  1/v_vec(1);
for i = 1:(n_nodes-1)
    b_vec = B(1:i,(i+1));
    W = [W + (b_vec * b_vec')/v_vec(i+1) , (-1)*b_vec/v_vec(i+1) ; (-1)*b_vec'/v_vec(i+1), 1/v_vec(i+1)];
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTE T_0:
T_0   = (v*(alpha-n_nodes-1))/(v+1) * inv(W);
return;




% function [A, DAG, Run] = struc_hc_pp(Data, data2, Priors_best, Pix_best);
% % [Run] = struc_hc_pp(Data, data2, Priors_best, Pix_best)
% % [Run, A] = hillclibming(Data, data2, Priors_best, Pix_best, Mu_best, Sigma_best)
% nbData=size(Data,1);
% global T_0;
% global my_0;
% global v;
% global alpha;
% global Prior;
% 
% [T_0, my_0, v, alpha, Prior] = SET_HYPERPARAMETERS(Data);
% [parents_candidate, ppscore]=process_clusters(data2, Priors_best, Pix_best);
% 
% % We set:
% steps = 200;
% step_iterations = 200;
% fan_in = zeros(1,size(Data,1)) + 8;
% 
% %% intial log_score
% A0=zeros(nbData, nbData);
% A0(find(data2==1))=1;
% A0(find(data2~=0))=1;
% A0(find(data2==Inf))=2;
% 
% A=A0;
% log_score = 0;
% pA = log(1);
% % for i=1:nbData
% %     for j=1:nbData
% %         if  A0(i,j)==0 && A(i,j)==0
% %             pA = pA + log(1-pd0);
% %         elseif A0(i,j)==0 && A(i,j)~=0
% %             pA = pA + log(pd0);
% %         elseif A0(i,j)~=0 && A(i,j)~=0
% %             pA = pA + log(pd1);
% %         elseif A0(i,j)~=0 && A(i,j)==0
% %             pA = pA + log(1-pd1);
% %         end
% %     end
% % end
% DAG = zeros(nbData, nbData);
% % [log_score_prior]=bnrc_prior(DAG, A); 
% log_score = pA ;%+ log_score_prior;
% [DAG, log_score_data, A_EDGE, R_EDGE] = struc_hc(step_iterations, DAG, fan_in, log_score, Data, ppscore, parents_candidate);
% log_score = log_score + log_score_data;
% 
% for i=1:steps+1
% Run.dag{i}         = 0;
% % Run.vector{i}      = 0;
% Run.Log_Scores(i)  = 0;
% Run.Simulation(i)  = 0;
% Run.A_EDGE(i)      = 0;
% Run.R_EDGE(i)      = 0;
% end  
% Run.dag{1}         = DAG;
% % Run.vector{1}      = vector;
% Run.Log_Scores(1)  = log_score;
% Run.steps(1)       = 1;
% Run.A_EDGE(1)      = A_EDGE;
% Run.R_EDGE(1)      = R_EDGE;
% %% [Run] = START_struc(DATA_ALL, steps, step_iterations,  Run, fan_in, ppscore);
% last           = Run.steps(1);          % index of last sampled dags
% DAG            = Run.dag{last};         % the current DAG
% % vector         = Run.vector{last};      % the current allocation vector
% log_score      = Run.Log_Scores(last);  % the current log score
% 
% [A, DAG, Run, log_score] = update_database(DAG, A, Run, steps, log_score, step_iterations, fan_in, Data, ppscore, parents_candidate);
% return;


% 
% function [DAG, log_score, a_EDGE, r_EDGE] = struc_hc(step_iterations, DAG, fan_in, log_score, Data, ppscore, parents_candidate); 
% % [DAG, log_score, A, a_EDGE, r_EDGE] = struc_hc(step_iterations, DAG, fan_in, log_score, Data, A, A0, Pix_best, Mu_best, Sigma_best); 
% 
% [nbData, nbVar] = size(Data);
% [n_parents, n_nodes] = size(DAG);
% a_EDGE  = 0;
% r_EDGE  = 0;
% tau1 = 1;
% %%%%%% Start the MCMC simulation:
% for t=1:step_iterations
%     %     if(x<=p_0) % PERFORM A STRUCTURE-MCMC MOVE
%     %randomly choose a child_node
%     nodes_indicis = randperm(n_nodes);
%     child_node    = nodes_indicis(1);
%     %record old parents
%     old_parents = DAG(:,child_node);
%     new_parents = old_parents;
%     parents_card_old = sum(old_parents);
%     parents_indicis_old = find(old_parents);
%     %update parents
%     if (parents_card_old<fan_in(1,child_node));
%         %randomly choose a parent_index to update
%         %% no parents constraints
%         %         neighbours_old = n_parents;
% %                 indicis = randperm(n_parents);
% %                 x = indicis(1);
%         %% having parents constraints
%         %         [parents_candidate]=process_clusters(Priors_best, Pix_best);
%         %         neighbours_old = parents_candidate{child_node,1};
%         %         indicis = randperm(size(parents_candidate{child_node,1},2));
%         indicis = randperm(size(parents_candidate{child_node,1},2));
%         x = parents_candidate{child_node,1}(indicis(1));
%         
%         parent_index = x;
%         % delete or add this parent node parent_index
%         new_parents(parent_index,1) = 1 - new_parents(parent_index,1);
%         
%         % accept or reject new_parents
%         % check bnrc(new_parents, child_node) reduce or not from bnrc(old_parents, child_node)
%         
%     else %(parents_card_old>fan_in(1,child_node))
%         % randomly delete a parent node parent_index
%         x = randperm(fan_in(1,child_node));
%         x = x(1);
%         parent_index = parents_indicis_old(x); % delete this parent node
%         new_parents(parent_index,1) = 0;
%         %         neighbours_old = parents_card_old; % = fan_in
%     end
%     % accept or reject new_parents
%     % check bnrc(new_parents, child_node) reduce or not from bnrc(old_parents, child_node)
%     
%     %     parents_card_new    = sum(new_parents);
%     %     if (parents_card_new<fan_in(1,child_node))
%     %         neighbours_new = fan_in(1,child_node); % = n_parents-1
%     %     else
%     %         neighbours_new = parents_card_new; % = fan_in
%     %     end
%     DAG_NEW = DAG;
%     DAG_NEW(:,child_node) = new_parents;
%     % check bnrc(new_parents, child_node) reduce or not from bnrc(old_parents, child_node)
%     %     local_score_new =bnrc_data(new_parents, child_node, Data, Pix_best, Mu_best, Sigma_best);
%     %     local_score_old =bnrc_data(old_parents, child_node, Data, Pix_best, Mu_best, Sigma_best);
%     local_score_new = bnrc_data(Data, child_node, DAG_NEW) + 2*tau1*size(find(DAG_NEW==1),1);%2*edgescore bnrc_prior(DAG_NEW, A); %edgescore = tau1*size(find(G==1),1);
%     local_score_old = bnrc_data(Data, child_node, DAG)+ 2*tau1*size(find(DAG==1),1);%bnrc_prior(DAG, A);
%     %     local_score_new =greedyhc(Data, DAG_NEW, A0, A0, Pix_best, Mu_best, Sigma_best);
%     %     local_score_old =greedyhc(Data, DAG, A0, A0, Pix_best, Mu_best, Sigma_best);
%     log_score_diff = local_score_new - local_score_old;
%     if log_score_diff < 0
%         % accept the move:
%         DAG = DAG_NEW;
%         a_EDGE = a_EDGE + 1;
%         log_score = log_score + log_score_diff;
%     else
%         r_EDGE           = r_EDGE + 1;
%     end
% end
% return
%     
