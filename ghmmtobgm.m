
% [bestcr, bestPix, bestP, bestnbStates, bestBIC, gammaA, Cscore, RCscore, Datas, threshold1, fan_in] = sfghmm(Data, clusters, mixtures, ppnet, c, r, goldmatrix, clue);
% [Run_fusion, Run_imoto, Run_uaiai, fusiontime, imototime, uaiaitime] = ghmmtobgm(Datas, ppimatrix, steps, step_iterations, fan_in, RCscore);


function [Run_fusion, Run_imoto, Run_uaiai, fusiontime, imototime, uaiaitime] = ghmmtobgm(Datas, ppimatrix, steps, step_iterations, fan_in, RCscore)

[nbData, nbVar] = size(Data);
% steps = 100;
% step_iterations = 500;
dag = zeros(nbData,nbData);
tic;   
[Run_fusion] = fusion_bge(Datas,steps,step_iterations, fan_in, RCscore, dag);
fusiontime = toc;
tic;
[Run_imoto] = imoto_pp_bge(Datas,steps,step_iterations, fan_in, ppimatrix, dag);
imototime = toc;
tic;
[Run_uaiai] = uaiai_pcomplex_bge(Datas,steps,step_iterations, fan_in, ppimatrix, dag);
uaiaitime = toc;
